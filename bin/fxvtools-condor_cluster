#!/usr/bin/env python


############################
#
#         imports
#
############################

import glob
import optparse
import os

import numpy

from gstlal import dagparts
from gstlal import inspiral_pipe
from gstlal import multichannel_datasource

############################
#
#        functions
#
############################

def comparison_node_gen(comparisonJob, dag, parent_nodes, options):
    dag_nodes = {}

    channels = multichannel_datasource.channel_dict_from_channel_file(options.channel_list).keys()

    # add a job for each channel to process & cluster triggers
    for idx, channel in enumerate(channels):
        outpath = options.output_dir + "/trg_comparison/trg_comparison_%04d" % (idx+1)
        dag_nodes[channel] = inspiral_pipe.generic_node(comparisonJob, dag, parent_nodes = parent_nodes,
            opts = {'file-format': options.file_format,
                    'trigger-type': 'omicron',
                    'channel': channel,
                    'start-time': options.start_time,
                    'end-time': options.end_time
            },
            input_files = {'trigger-dir': options.trigger_dir},
            output_files = {'output-dir': outpath}
        )

        if options.verbose:
            print "Job %04d for rate %d" % (idx+1, rate)

    return dag_nodes

def parse_command_line():
    parser = optparse.OptionParser()

    # add some options
    parser.add_option("-m", "--trigger-dir", help = "Specify directory containing trigger files. All triggers will be plotted unless GPS time range is specified with --start-time and --stop-time options.")
    parser.add_option("--file-format", default = "xml", help = "Specify the file format of omicron triggers to be read, default = xml")
    parser.add_option("-o", "--output-dir", action = "store", default = ".", help = "Specify location for output files. Default is current directory.")	
    parser.add_option("-v", "--verbose", action = "store_true", default = False, help = "Be verbose.")
    parser.add_option("-s", "--start-time", type = "int", help = "Plot data for triggers starting at specified GPS time. Specified time must agree with times in trigger files.") 
    parser.add_option("-e", "--end-time", type = "int", help = "Plot data for triggers up to specified GPS time. Specified time must agree with times in trigger files.") 
    parser.add_option("--channel-list", type="string", metavar = "name", help = "Set the list of the channels to process. Command given as --channel-list=location/to/file")
 
	# Condor commands
    parser.add_option("--request-cpu", default = "2", metavar = "integer", help = "set the job CPU count, default = 1")
    parser.add_option("--request-memory", default = "4GB", metavar = "integer", help = "set the job memory, default = 2GB")
    parser.add_option("--condor-command", action = "append", default = [], metavar = "command=value", help = "set condor commands of the form command=value; can be given multiple times")

    options, filenames = parser.parse_args()

    return options, filenames


############################
#
#           main
#
############################

if __name__ == "__main__":
    # parse options
    options, filenames = parse_command_line()

    # DAG setup
    try:
        os.mkdir("logs")
    except OSError:
        pass

    dag = inspiral_pipe.DAG("trg_comparison")

    # setup the job classes
    # FIXME: don't hardcode path to exec
    comparisonJob = inspiral_pipe.generic_job("fxvtools-cluster", tag_base = 'trg_comparison', condor_commands = inspiral_pipe.condor_command_dict_from_opts(options.condor_command, {"request_memory":options.request_memory, "request_cpus":options.request_cpu, "want_graceful_removal":"True", "kill_sig":"15"}))

    # generate comparison nodes
    comparison_nodes = comparison_node_gen(comparisonJob, dag, [], options)

    # generate DAG
    dag.write_sub_files()
    dag.write_dag()
    dag.write_script()


