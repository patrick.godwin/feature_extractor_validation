__description__ = "a python module with plotting utilities"
__author__ = "Sydney Chamberlin (sydney.chamberlin@ligo.org), Patrick Godwin (patrick.godwin@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])

#-------------------------------------------------

import datetime
import os
import sys
from textwrap import wrap

import numpy
from scipy import stats

import matplotlib
matplotlib.use('Agg')
from mpl_toolkits.axes_grid import make_axes_locatable
from matplotlib.colorbar import Colorbar
from matplotlib import pyplot as plt
from matplotlib import ticker as tkr
import matplotlib.cm as cm

from lal import GPSToUTC

#-------------------------------------------------
### default styles/colors

matplotlib.rcParams.update({
	"font.size": 10.0,
	"axes.titlesize": 10.0,
	"axes.labelsize": 10.0,
	"xtick.labelsize": 8.0,
	"ytick.labelsize": 8.0,
	"legend.fontsize": 8.0,
	"text.usetex": True,
	"figure.dpi": 200,
	"savefig.dpi": 200
})

colors = ['#2c7fb8', '#e66101', '#5e3c99', '#d01c8b']

#-------------------------------------------------
### utilities for plotting

def save_figure(name, fig, key, outpath):
	key = '_'.join(key)

	fname = name+'.png'
	figpath = os.path.join(key, fname)
	fig.savefig(os.path.join(outpath, fname))
	plt.close(fname)
	return (key, figpath)

def determine_time_scale(tmin, tmax):
	time_int = tmax-tmin
	nticks = 6
	#FIXME: make this scaling more robust, this is kind of hackish
	# use hours
	if time_int > 6400.:
		hourtime = time_int/3600.
		stepsize = numpy.ceil(hourtime/nticks)
		tickrange = numpy.arange(tmin, tmax, stepsize*3600.)
		ticklabelrange = numpy.arange(0, hourtime, stepsize) 
		axlabel = 'hours'

	# use minutes
	elif time_int < 6400.:
		minutetime = time_int/60.
		stepsize = numpy.ceil(minutetime/nticks)
		tickrange = numpy.arange(tmin, tmax, stepsize*60.)
		ticklabelrange = numpy.arange(0, minutetime, stepsize)
		axlabel = 'minutes'	

	return tickrange, ticklabelrange, axlabel

#-------------------------------------------------
### plots

def missed_found(found_t, found_snr, extra_trg1_t, extra_trg1_snr, extra_trg2_t, extra_trg2_snr, names):

	# parse names into inputs
	ch_key, trg1, trg2 = names

	gps_start, gps_end = (min(numpy.min(found_t), numpy.min(extra_trg1_t)), max(numpy.max(found_t), numpy.max(extra_trg1_t)))
	
	xticks, xticklabelmarks, xaxtimelabel = determine_time_scale(gps_start, gps_end)
	datetimefmt = datetime.datetime(*GPSToUTC(int(gps_start))[:7]).strftime("%Y-%m-%d, %H:%M:%S UTC") + " (" + str(gps_start).split('.')[0]+")"
	
	fig, axes = plt.subplots()
	
	axes.scatter(found_t, found_snr, color = 'k', alpha = 0.4, label = "Found")
	axes.scatter(extra_trg1_t, extra_trg1_snr, color = '#4C627D', alpha = 0.4, label = "Missed %s" % trg1)
	axes.scatter(extra_trg2_t, extra_trg2_snr, marker='+', color='r', alpha = 0.4, label = "Missed %s" % trg2)
	
	axes.set_title(r"SNR missed/found plot for channel %s" % ch_key)
	# FIXME: scale time correctly on the bottom
	axes.set_xlabel(r"Time [%s] from %s" % (xaxtimelabel,datetimefmt))
	axes.set_ylabel(r"SNR (Omicron)")
	axes.legend(loc='upper right', shadow=True)
	axes.set_xticks(xticks)
	axes.set_xticklabels(xticklabelmarks)
	
	return fig

def snr_hist(paramstr, trg1_snrs, trg2_snrs, names, snr_cutoff):

	# parse names into inputs
	ch_key, trg1, trg2 = names

	# settings
	n_bins = 100
	max_snr = 150
	#max_snr = max(max(trg1_snrs), max(trg2_snrs))

	fig, axes = plt.subplots()

	axes.hist(trg1_snrs, numpy.linspace(snr_cutoff, max_snr, num=n_bins), histtype='bar', color=colors[0], alpha=0.5, label = trg1, log=True)
	axes.hist(trg2_snrs, numpy.linspace(snr_cutoff, max_snr, num=n_bins), histtype='bar', color=colors[1], alpha=0.5, label = trg2, log=True)

	title = axes.set_title("\n".join(wrap(r"%s for Channel %s" % (paramstr, ch_key))))
	title.set_y(1.05)

	axes.set_title(r"%s for Channel %s" % (paramstr, ch_key))
	axes.set_xlabel(r"SNR")
	axes.set_ylabel(r"Counts")
	axes.legend(loc='upper right', shadow=True)
	axes.axis([0, 150, 0.1, 10**3])

	return fig

def delta_t_hist(paramstr, times, time_window, ch_key, scale=None):

	# settings
	#n_bins = 20
	n_bins = 100

	fig, axes = plt.subplots()
	
	#axes.hist(times, numpy.linspace(-0.5*time_window, 0.5*time_window, num=n_bins), histtype='bar', alpha=0.7, color=colors[2], normed=True)
	axes.hist(times, bins=n_bins, alpha=0.7, color=colors[2], log=True, range=(-0.01, 0.01))
	#axes.hist(times, bins=n_bins, alpha=0.7, color=colors[2], log=True, range=(-0.05, 0.05))

	title = axes.set_title("\n".join(wrap(r"%s for Channel %s" % (paramstr, ch_key))))
	title.set_y(1.05)
	
	axes.set_title(r"%s histogram for channel %s" % (paramstr, ch_key))
	axes.set_xlabel(r"Trigger time - injection time (s)")
	axes.set_ylabel(r"Number of triggers")
	axes.axis([-0.01, 0.01, 0.1, 10**4])
	#axes.axis([-0.05, 0.05, 0.1, 10**4])

	return fig

def found_params(paramstr, trg1_time, trg1_params, trg2_time, trg2_params, names, scale=None):

	# parse names into inputs
	ch_key, trg1, trg2 = names

	gps_start, gps_end = (min(numpy.min(trg1_time), numpy.min(trg2_time)), max(numpy.max(trg1_time), numpy.max(trg2_time)))

	if trg1_params.size > 0 and trg2_params.size > 0:
		# put a line on plot to indicate 1-1 correspondence 
		maxparam_val = max(max(trg1_params), max(trg2_params))
		minparam_val = min(min(trg1_params), min(trg2_params))

	else:
		maxparam_val = 1.
		minparam_val = 0.
		
	r_squared = (stats.pearsonr(trg1_params, trg2_params)[0][0])**2

	fig, axes = plt.subplots()
	
	axes.scatter(trg1_params, trg2_params, color = colors[2], alpha = 0.4)

	# set scale if specified, otherwise linear
	if scale == 'loglog':
		axes.loglog()

	axes.plot(numpy.linspace(minparam_val,maxparam_val),numpy.linspace(minparam_val,maxparam_val), color = 'k', label="$R^{2}$ = %f" %r_squared)
	
	title = axes.set_title("\n".join(wrap(r"%s for Channel %s" % (paramstr, ch_key))))
	title.set_y(1.05)
	axes.set_xlabel(r"%s %s" % (trg1, paramstr))
	axes.set_ylabel(r"%s %s" % (trg2, paramstr))
	axes.legend(loc='upper right', shadow=True)
	
	return fig

def found_params_vs_time(paramstr, trg1_time, trg1_params, trg2_time, trg2_params, names, scale=None):

	# parse names into inputs
	ch_key, trg1, trg2 = names

	gps_start, gps_end = (min(numpy.min(trg1_time), numpy.min(trg2_time)), max(numpy.max(trg1_time), numpy.max(trg2_time)))
	
	xticks, xticklabelmarks, xaxtimelabel = determine_time_scale(gps_start, gps_end)
	datetimefmt = datetime.datetime(*GPSToUTC(int(gps_start))[:7]).strftime("%Y-%m-%d, %H:%M:%S UTC") + " (" + str(gps_start).split('.')[0]+")"
	
	fig, axes = plt.subplots()

	if scale=='semi_logy':
		axes.semilogy()
	
	axes.scatter(trg1_time, trg1_params, color = '#4C627D', alpha = 0.4, label = trg1)
	axes.scatter(trg2_time, trg2_params, color = 'k', alpha = 0.4, label = trg2)
	
	axes.set_title(r"%s vs time for Channel %s" % (paramstr, ch_key))
	axes.set_xlabel(r"Time [%s] from %s" % (xaxtimelabel,datetimefmt))
	axes.set_ylabel(r"%s" % paramstr)
	axes.legend(loc='upper right', shadow=True)
	axes.set_xticks(xticks)
	axes.set_xticklabels(xticklabelmarks)
	
	return fig

def inj_detected_param(param, inj_param, trg_param, names):

	# parse names into inputs
	ch_key, inj, trg = names

	fig, axes = plt.subplots()

	# plot middle dotted line
	x=[1,2,10**11]
	y=[1,2,10**11]
	axes.plot(x,y, '--', color='blue')

	# plot data
	axes.scatter(inj_param, trg_param, marker = 'o', color = colors[2], alpha = 0.4)

	# plot confidence bounds
	if param == 'snr':
		x1 = range(1,200)
		y1 = [numpy.sqrt(2)*xi for xi in x1]
		x2 = [1,2,200]
		y2 = [0.70710678118, 1.41421356237, 141.421356237]
		x3 = [1, 200]
		y3 = [5.5, 5.5]
		axes.plot(x3, y3, '-', color='red')

	elif param == 'frequency':
		x1 = [0, 1, 2, 3000]
		y1 = [0, 1.1, 2.2, 3300]
		x2 = [1, 20, 50, 100, 3000]
		y2 = [0.9, 18, 45, 90, 2700]
	else:
		raise NotImplementedError

	axes.plot(x1, y1, '--', color='gray')
	axes.plot(x2, y2, '--', color='gray')

	axes.set_xlabel('Injected %s'%param)
	axes.set_ylabel('Detected %s'%param)
	axes.set_title('%s injected vs. detected %s' %(trg, param))
	axes.loglog()

	# set axis limits
	if param == 'snr':
		axes.axis([3, 140, 3, 140])
	elif param == 'frequency':
		axes.axis([25, 5000, 25, 5000])

	# add gridlines
	axes.grid(True)
	
	return fig

def inj_missed_found_param_vs_time(param, found_t, found_param, missed_t, missed_param, names):

	# parse names into inputs
	ch_key, trg1, trg2 = names

	gps_start, gps_end = (min(numpy.min(found_t), numpy.min(missed_t)), max(numpy.max(found_t), numpy.max(missed_t)))

	xticks, xticklabelmarks, xaxtimelabel = determine_time_scale(gps_start, gps_end)
	datetimefmt = datetime.datetime(*GPSToUTC(int(gps_start))[:7]).strftime("%Y-%m-%d, %H:%M:%S UTC") + " (" + str(gps_start).split('.')[0]+")"

	fig, axes = plt.subplots()

	axes.scatter(found_t, found_param, marker = 'o', color = colors[2])
	axes.scatter(missed_t, missed_param, marker = 'x', color = 'red', s=70)

	axes.set_title(r"%s missed/found plot vs. time for channel %s" % (ch_key, param))
	# FIXME: scale time correctly on the bottom
	axes.set_xlabel(r"Time [%s] from %s" % (xaxtimelabel,datetimefmt))
	axes.set_ylabel(r"Injected %s" % param)
	axes.legend(('Found injections', 'Missed injections'), loc='upper right')
	axes.set_xticks(xticks)
	axes.set_xticklabels(xticklabelmarks)

	return fig

def inj_missed_found(found_snr, found_freq, missed_snr, missed_freq, names):

	# parse names into inputs
	ch_key, inj, trg = names

	fig, axes = plt.subplots()

	#axes.scatter(found_snr, found_freq, marker = 'o', color = 'blue')
	axes.scatter(found_snr, found_freq, marker = 'o', color = colors[2])
	axes.scatter(missed_snr, missed_freq, marker = 'x', color = 'red', s=70)
	
	axes.set_xlabel('Injected SNR')
	axes.set_ylabel('Injected frequency (Hz)')
	axes.set_title('%s missed %s found %s' %(trg,len(missed_snr),len(found_snr)))
	axes.loglog()
	axes.axis([5, 100, 10, 4000])
	axes.legend(('Found injections', 'Missed injections'), 'lower right')
	axes.grid(True)

	return fig

def inj_param_diff(param, inj_param, trg_param, names):

	# parse names into inputs
	ch_key, inj, trg = names

	fig, axes = plt.subplots()

	diff_param = ((trg_param - inj_param) / inj_param)

	# plot bounds of freq matching
	if param == 'frequency':
		x1 = [1, 100, 5000]
		x2 = [1, 100, 5000]
	elif param == 'snr':
		x1 = [1, 10, 100]
		x2 = [1, 10, 100]

	y1 = [0.1, 0.1, 0.1]
	y2 = [-0.1, -0.1, -0.1]

	#plt.hist(EP_time_diff_plot, bins=50,  color='blue')
	axes.scatter(inj_param, diff_param, marker = 'x', color = colors[2])
	axes.plot(x1, y1, '--', color = 'gray')
	axes.plot(x1, y2, '--', color = 'gray')
	axes
	axes.set_xlabel('Injection %s' % param)
	axes.set_ylabel('(Trigger %s - Inj %s) over Inj %s' % (param, param, param))
	axes.set_title('%s percent difference in injected %s vs. injected %s' %(trg, param, param))
	axes.semilogx()

	# set lims
	if param == 'frequency':
		#axes.axis([30, 2500, -0.15, 0.15])
		#axes.axis([30, 4000, -1, 1])
		axes.axis([30, 4000, -0.5, 0.5])
	elif param == 'snr':
		#axes.axis([5, 100, -0.15, 0.15])
		#axes.axis([5, 100, -5, 5])
		axes.axis([5, 100, -1, 1])
	else:
		raise NotImplementedError

	axes.grid(True)

	return fig

def inj_param_diff_snr(param, inj_param, trg_param, inj_snr, names):

	# parse names into inputs
	ch_key, inj, trg = names
	param_name = param.replace('_', ' ')

	fig, axes = plt.subplots()

	diff_param = (trg_param - inj_param)

	axes.scatter(inj_snr, diff_param, marker = 'x', color = colors[2])

	axes.set_xlabel('Injected SNR')
	axes.set_ylabel('(Trigger %s - Injection %s)' % (param_name, param_name))
	axes.set_title('%s %s difference vs. SNR' %(trg, param_name))
	axes.semilogx()

	# set lims
	if param == 'trigger_time':
		axes.axis([5, 100, -0.1, 0.1])
		#axes.axis([5, 100, -2, 2])
	elif param == 'frequency':
		axes.axis([5, 100, -500, 500])
	else:
		raise NotImplementedError

	axes.grid(True)

	return fig
