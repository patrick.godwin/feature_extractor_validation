__description__ = "a python module with utilities for clustering of features"
__author__ = "Patrick Godwin (patrick.godwin@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])

#-------------------------------------------------

import itertools
import sys

import numpy

from fxvtools import utils

#-------------------------------------------------
### utilities to aid in clustering

def cluster_features(features, time_window=1.0, clustering='max'):
	"""
	Given a list of triggers that can be keyed by their respective columns,
	will cluster a set of features in time.

	This clustered algorithm is an adaptation of `lalburst/bucluster.py`.

	Parameters
	----------
	features : `list` of `numpy.array`
		features in which to cluster

	time_window : `float`, optional
		The window (in s) in which to cluster features in time
		Default = 1.0

	clustering : `str`, optional
		Defines how to do clustering, possible options:
		* max: loudest snr trigger within window
		* snr: snr-weighted clustering
		Default = max

	Returns
	-------
	clustered_features : `list` of `numpy.array`
		the clustered set of features
	"""
	### if zero or one feature, nothing to cluster
	if len(features) < 2:
		return features

	### sort features in time and prep needed properties
	idx = 0
	features = sorted(features, key=lambda x: x['trigger_time'])
	a = list(features)

	### cluster features
	for b in itertools.islice(features, 1, None):

		### nothing to cluster, skip to next feature
		if utils.in_new_epoch(b['trigger_time'], a[idx]['trigger_time'], time_window):
			idx += 1

		else:
			if clustering == 'max':
				if a[idx]['snr'] >= b['snr']:
					del a[idx+1]
				else:
					del a[idx]

			elif clustering == 'snr':
				### Compute SNR squared weighted trigger time and frequency
				a[idx]['trigger_time'] = (a[idx]['snr']**2.0 * a[idx]['trigger_time'] + b['snr']**2.0 * b['trigger_time']) / (a[idx]['snr']**2.0 + b['snr']**2.0)
				a[idx]['frequency'] = (a[idx]['snr']**2.0 * a[idx]['frequency'] + b['snr']**2.0 * b['frequency']) / (a[idx]['snr']**2.0 + b['snr']**2.0)

				### compute rms SNR
				a[idx]['snr'] = numpy.sqrt(a[idx]['snr']**2.0 + b['snr']**2.0)

				### remove this feature from the result
				del a[idx+1]

	return a
