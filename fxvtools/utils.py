__description__ = "a python module with some basic utilities"
__author__ = "Patrick Godwin (patrick.godwin@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])

#-------------------------------------------------

import os
import sys
import urlparse

import numpy

from glue import markup

#-------------------------------------------------
### gps time utilities borrowed from gstlal-ugly

def in_new_epoch(new_gps_time, prev_gps_time, gps_epoch):
	"""
	Returns whether new and old gps times are in different
	epochs.
	"""
	return (new_gps_time - floor_div(prev_gps_time, gps_epoch)) >= gps_epoch

def floor_div(x, n):
	"""
	Floor an integer by removing its remainder
	from integer division by another integer n.
	e.g. floor_div(163, 10) = 160
	e.g. floor_div(158, 10) = 150
	"""
	assert n > 0
	return (x // n) * n

#-------------------------------------------------
### validation utilities

def find_matches(trg1, trg2, time_window):
	# get times
	A = trg1['trigger_time']
	B = trg2['trigger_time']

	# sort and set mask to determine closest triggers
	sidx_B = B.argsort()
	sorted_B = B[sidx_B]
	sorted_idx = numpy.searchsorted(sorted_B, A)
	sorted_idx[sorted_idx==B.size] = B.size-1
	mask = (sorted_idx > 0) &((numpy.abs(A - sorted_B[sorted_idx-1]) < numpy.abs(A - sorted_B[sorted_idx])) )

	# determine indices and residuals
	idxs_A = numpy.arange(A.size)
	idxs_B = sidx_B[sorted_idx-mask]
	residuals = A - B[idxs_B]

	# find all matches (if two in same window, pick one with higher SNR)
	matches = {}
	for idx_A, idx_B, residual in zip(idxs_A, idxs_B, residuals):
		if numpy.abs(residual) < time_window/2.:
			if idx_B not in matches:
				matches[idx_B] = (idx_A, residual)
			elif trg1[idx_A]['snr'] > trg1[matches[idx_B][0]]['snr']:
				matches[idx_B] = (idx_A, residual)

	# prep outputs for readability
	A_idxs, residuals = zip(*matches.values())
	B_idxs = matches.keys()

	return numpy.array(A_idxs, dtype=int), numpy.array(B_idxs, dtype=int), numpy.array(residuals, dtype=float)

#-------------------------------------------------
### web utilities

cluster_urls = {
	'CIT': 'https://ldas-jobs.ligo.caltech.edu/',
	'LHO': 'https://ldas-jobs.ligo-wa.caltech.edu/',
	'LLO': 'https://ldas-jobs.ligo-la.caltech.edu/',
	'UWM': 'https://ldas-jobs.cgca.uwm.edu/'
}

def to_output_url(output_dir, cluster):
	username = os.getlogin()
	basepath = os.path.join(os.path.join('/home/', username), 'public_html')
	extension_url = os.path.relpath(os.path.abspath(output_dir), basepath)
	base_url = urlparse.urljoin(cluster_urls[cluster], '~' + username)
	return base_url + '/' + extension_url

def generate_html_file(output_dir, cluster, plot_paths, plot_strs):

	# grab comparison names
	names, _ = plot_paths[0]
	ch, trg1, trg2 = names.split('_')

	job_ids = set()
	#
	### head
	#
	title = "%s - %s comparison" % (trg1, trg2)
	metainfo = {'charset': 'utf-8', 'name': 'viewport', 'content': 'width=device-width, initial-scale=1'}
	doctype = '<!DOCTYPE html>'
	css = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'
	bootstrap = ['https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js']

	page = markup.page()
	page.init(title = title, metainfo = metainfo, doctype = doctype, css = css, script = bootstrap)

	#
	### body
	#
	page.div(class_ = 'container')

	# header
	page.h2('%s - %s Comparison' % (trg1, trg2))
	page.hr()
	page.p('Run over 100000 seconds of MDC data, containing white noise burst and sine-gaussian injections')
	page.br()
	page.hr()

	# plots
	plot_paths = sorted(plot_paths, key=lambda x: x[1], reverse=True)
	for key in plot_paths:
		job_id, plot = key
		ch = job_id.split('_')[0]
		plot_url = to_output_url(output_dir, cluster) + '/' + plot
		if job_id not in job_ids:
			job_ids.add(job_id)
			page.div(_class = 'col-md-12')
			page.h4('Results for channel %s' % ch, align = 'center')
			for line in plot_strs[ch].split('\n'):
				page.p(line)
			page.div.close()
		page.div(_class = 'col-md-6')
		page.div(_class = 'thumbnail')
		page.a(markup.oneliner.img(src = plot_url, alt = '', style = 'width:100%', _class='img-responsive'), href = plot_url, target = '_blank')
		page.div.close()
		page.div.close()
	#
	### generate page
	#
	page.div.close()
	with open(os.path.join(output_dir, 'index.html'), 'w') as f:
		print >> f, page
