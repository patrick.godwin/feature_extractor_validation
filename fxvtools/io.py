# Copyright (C) 2017 Sydney J. Chamberlin, Patrick Godwin
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# =============================================================================
#
#                               PREAMBLE
#
# =============================================================================

import sys, os, glob
from collections import defaultdict
from collections import namedtuple
import datetime
import itertools
import math
import string

import h5py
import numpy

import lal
from lal import CachedDetectors
from lal import TimeDelayFromEarthCenter

from glue.ligolw import ligolw
from glue.ligolw import lsctables
from glue.ligolw import param
from glue.ligolw import utils as ligolw_utils

from fxvtools import cluster
from fxvtools import utils

# =============================================================================
#
#                               CLASSES
#
# =============================================================================

class GstlalData(object):

	def __init__(self, triggerdir, start_time = None, end_time = None, preloaded_triggers = False, cluster_window = 1, cluster_type=None, file_format = 'hdf5', verbose = True):

		self.triggerdir = triggerdir
		self.start_time = start_time
		self.end_time = end_time
		self.cluster_window = cluster_window
		self.cluster_type = cluster_type
		self.file_format = file_format
		self.preloaded_triggers = preloaded_triggers
		self.verbose = verbose

		# populate data structure with relevant triggers
		self.triggerdict = {}
		self._extract_triggers()

	def filter_triggers(self, column = 'snr', threshold = 8.0):
		"""
		filter triggers in place based on some threshold value and a column,
		default is snr with a threshold of 8.0
		"""
		for key in self.triggerdict.keys():
			filter_idx = numpy.where(self.triggerdict[key][column] >= threshold)
			self.triggerdict[key] = self.triggerdict[key][filter_idx]

	def sort_triggers(self, column = 'snr', descending = True):
		"""
		returns a sorted list of descending triggers based on column,
		in the form of (row, channel)
		"""
		rows = [(row, channel) for channel, value in self.triggerdict.items() if value.shape[0] > 0 for row in numpy.split(value, value.shape[0], axis=0)]
		return [row for row in sorted(rows, key=lambda e: e[0][column][0], reverse=descending)] 

	def _extract_triggers(self):
		"""
		helper method to extract triggers from disk given some file format, default is 'ascii', but also accepts 'hdf5'
		"""

		if self.file_format == 'hdf5':
			# grab relevant trigger files for each gps range
			interval = self.end_time - self.start_time
			gps_idx = len(str(self.end_time)) - len(str(interval))

			glob_exp = '*/*/*/*'
			for idx in range(len(str(self.end_time))):
				if idx == gps_idx:
					effective_start_time = self.start_time
					glob_exp += '[' + str(effective_start_time)[idx] + '-' + str(self.end_time - 1)[idx] + ']'
				else:
					glob_exp += '[0-9]'
			glob_exp += '*.h5'

			# open hdf file and find datasets corresponding to a specific channel and rate
			save_cadence = None
			row_dtypes = None

			# get all files by gps range
			#hdf_paths = sorted(glob.glob(os.path.join(self.triggerdir, glob_exp)))
			hdf_paths = sorted(glob.glob(os.path.join(self.triggerdir, '*/*/*/*.h5')))
			print("paths: %s" %repr(self.triggerdir))
			print("glob: %s" %repr(glob_exp))

			# start reading through each file
			for hdf_path in hdf_paths:

				if self.verbose:
					print >>sys.stderr, "Reading h5 file from: %s" % hdf_path

				# find out which hdf groups to get from file
				with h5py.File(hdf_path, 'r') as hfile:
					channels = hfile.keys()

					for channel in channels:
						if self.verbose:
							print >>sys.stderr, "Processing triggers from channel: %s" % channel

						if self.preloaded_triggers:
							dataset = hfile[channel].keys()[0]
							self.triggerdict[channel] = numpy.array(hfile[channel][dataset])
						else:
							# get all rates and datasets for a given channel
							# assumes that all rates for a given channel have the same datasets associated with them
							datasets = hfile[channel].keys()

							if datasets:
								# grab relevant datasets based on gps range
								# FIXME: think about start time here
								relevant_datasets = [dataset for dataset in datasets if self.start_time <= int(dataset.split('_')[0]) <= self.end_time]

								# grab save cadence and numpy dtypes for datasets
								# FIXME: should be using hdf metadata for this instead
								if not (save_cadence and row_dtypes):
									save_cadence = int(datasets[0].split('_')[1])
									row_dtypes = hfile[os.path.join(channel, relevant_datasets[0])].dtype

								# loop through each dataset
								for idx, dataset in enumerate(relevant_datasets):
									if self.verbose:
										print >>sys.stderr, "Processing triggers from dataset: %s" % dataset

									hdf_keys = [os.path.join(channel, dataset)]

									# combine datasets for all rates
									rows = numpy.concatenate([numpy.array(hfile[key]) for key in hdf_keys if key in hfile], axis=0)

									# remove all rows with nan
									nonnan_indices = numpy.where(numpy.isfinite(rows['snr']))
									rows = rows[nonnan_indices]

									# cluster features together
									rows = [row for row in rows]
									if self.cluster_type:
										rows = numpy.array(cluster.cluster_features(rows, time_window=self.cluster_window, clustering=self.cluster_type), dtype=row_dtypes)
									else:
										rows = numpy.array(rows, dtype=row_dtypes)

									# append to trigger dict
									if channel in self.triggerdict:
										self.triggerdict[channel] = numpy.concatenate((self.triggerdict[channel], rows), axis=0)
									else:
										self.triggerdict[channel] = rows

			# convert to numpy recarray
			for channel in self.triggerdict.keys():
				self.triggerdict[channel] = self.triggerdict[channel].view(numpy.recarray)

		else:
			raise NotImplementedError

	def items(self):
		return self.triggerdict.items()

	def keys(self):
		return self.triggerdict.keys()

	def values(self):
		return self.triggerdict.values()

	def __len__(self):
		return len(self.triggerdict)

	def __getitem__(self, key):
		return self.triggerdict[key]

	def __contains__(self, key):
		return key in self.triggerdict

	def __repr__(self):
		return repr(self.triggerdict)

class OmicronData(object):

	def __init__(self, triggerdir, start_time = None, end_time = None, preloaded_triggers = False, cluster_window = 1, cluster_type = 'max', file_format = 'xml', verbose = True, channel = None, trigger_type = 'MDC'):
		"""
		Returns object with various omicron trigger parameters.
		"""
		self.triggerdir = triggerdir
		#FIXME: make sure channel name has same format for gstlal and omicron triggers
		self.start_time = start_time
		self.end_time = end_time
		self.cluster_window = cluster_window
		self.cluster_type = cluster_type
		self.file_format = file_format
		self.preloaded_triggers = preloaded_triggers
		self.trigger_type = trigger_type
		self.verbose = verbose
		self.channel = channel

		# populate data structure with relevant triggers
		self.triggerdict = {}
		self._extract_triggers()
	
	def filter_triggers(self, column = 'snr', threshold = 8.0):
		"""
		filter triggers in place based on some threshold value and a column,
		default is snr with a threshold of 8.0
		"""
		for key in self.triggerdict.keys():
			filter_idx = numpy.where(self.triggerdict[key][column] >= threshold)
			self.triggerdict[key] = self.triggerdict[key][filter_idx]

	def sort_triggers(self, column = 'snr', descending = True):
		"""
		returns a sorted list of descending triggers based on column,
		in the form of (row, key) where key = (gps_range, channel, rate)
		"""
		rows = [(row, key) for key, value in self.triggerdict.items() if value.shape[0] > 0 for row in numpy.split(value, value.shape[0], axis=0)]
		return [row for row in sorted(rows, key=lambda e: e[0][column][0], reverse=descending)]

	def _extract_triggers(self):
		"""
		helper method to extract triggers from disk given omicron trigger files, either ascii, xml or previously saved triggers in hdf5
		"""
		# Define named structures for clarity
		Row = namedtuple('Row', ['trigger_time', 'snr', 'frequency', 'phase', 'q'])
		row_dtype = [(column, 'f8') for column in Row._fields]
	
		if self.file_format == 'xml':

			# define a content handler
			class LIGOLWContentHandler(ligolw.LIGOLWContentHandler):
			    pass

			lsctables.use_in(LIGOLWContentHandler)

			# grab relevant trigger files for each gps range
			interval = self.end_time - self.start_time
			gps_idx = len(str(self.end_time)) - len(str(interval))

			# if a channel is specified, grab only those files instead
			if self.channel:
				if self.trigger_type == 'production':
					ifo, channel = self.channel.split(':')
					channel = channel.replace('-', '_')+'_OMICRON'
					glob_exp = '%s/*/*' % channel
				elif self.trigger_type == 'MDC':
					glob_exp = '%s/*' % self.channel
			else:
				if self.trigger_type == 'production':
					glob_exp = '*/*/*'
				elif self.trigger_type == 'MDC':
					glob_exp = '*/*'

			for idx in range(len(str(self.end_time))):
				if idx == gps_idx:
					effective_start_time = self.start_time
					glob_exp += '[' + str(effective_start_time)[idx] + '-' + str(self.end_time - 1)[idx] + ']'
				else:
					glob_exp += '[0-9]'

			if self.trigger_type == 'production':
				glob_exp += '*.xml.gz'
			elif self.trigger_type == 'MDC':
				glob_exp += '*.new.xml.gz'
			print >>sys.stderr,  os.path.join(self.triggerdir, glob_exp)

			# extract trigger data and match column headings to data
			trgfiles = sorted(glob.iglob(os.path.join(self.triggerdir, glob_exp)))

			# construct trigger dictionary
			for trgfile in trgfiles:

				# load xml doc
				xmldoc = ligolw_utils.load_filename(trgfile, contenthandler = LIGOLWContentHandler, verbose = self.verbose)

				# get tabular format (sngl burst for omicron)
				sngl_burst_table = lsctables.SnglBurstTable.get_table(xmldoc)

				if self.channel:
					channel = self.channel
				else:
					process_params_table = lsctables.ProcessParamsTable.get_table(xmldoc)

					# get channel name and rate
					channel = [row.param for row in process_params_table if row.value == 'omicron_DATA_CHANNEL'][0]

				# preallocate space and initialize all values to nan
				rows = []

				for idx, row in enumerate(sngl_burst_table):
					# get central time of trigger
					trg_time = row.peak_time + row.peak_time_ns * 1e-9

					# filter out rows that don't span the gps range
					if trg_time >= int(self.start_time) and trg_time <= int(self.end_time) + 1:

						if self.trigger_type == 'production':
							trigger_row = Row(trigger_time = trg_time, snr = row.snr, frequency = row.central_freq, phase = row.param_one_value, q = 0)
						elif self.trigger_type == 'MDC':
							trigger_row = Row(trigger_time = trg_time, snr = row.snr, frequency = row.central_freq, phase = 0, q = 0)

						rows.append(trigger_row)

				# cluster features together
				rows = cluster.cluster_features(numpy.array(rows, dtype=row_dtype), time_window = self.cluster_window)

				# append to trigger dict
				if channel in self.triggerdict:
					self.triggerdict[channel] = numpy.concatenate((self.triggerdict[channel], numpy.array(rows, dtype=row_dtype)), axis=0)
				else:
					self.triggerdict[channel] = numpy.array(rows, dtype=row_dtype)

			for channel in self.triggerdict.keys():
				# remove all rows with nan
				nonnan_indices = numpy.where(numpy.isfinite(self.triggerdict[channel]['snr']))
				self.triggerdict[channel] = self.triggerdict[channel][nonnan_indices]

				# convert to numpy recarray
				self.triggerdict[channel] = self.triggerdict[channel].view(numpy.recarray)

		elif self.file_format == 'hdf5':
			interval = self.end_time - self.start_time
			gps_idx = len(str(self.end_time)) - len(str(interval))

			glob_exp = '*/*/*/*'
			for idx in range(len(str(self.end_time))):
				if idx == gps_idx:
					effective_start_time = self.start_time
					glob_exp += '[' + str(effective_start_time)[idx] + '-' + str(self.end_time - 1)[idx] + ']'
				else:
					glob_exp += '[0-9]'
			glob_exp += '*.h5'

			# get all files by gps range
			hdf_paths = glob.glob(os.path.join(self.triggerdir, glob_exp))

			# start reading through each file
			for hdf_path in hdf_paths:

				# open hdf file and find datasets corresponding to a specific channels
				with h5py.File(hdf_path, 'r') as hfile:
					# find out channels to get from file
					channels = hfile.keys()

					save_cadence = None
					row_dtypes = None

					for channel in channels:
						if self.verbose:
							print >>sys.stderr, "Processing triggers from channel: %s" % channel

						# find datasets corresponding to each group
						datasets = hfile[channel].keys()

						if datasets:
							# grab save cadence and numpy dtypes for datasets
							# FIXME: should be using hdf metadata for this instead
							if not (save_cadence and row_dtypes):
								save_cadence = int(datasets[0].split('_')[1])
								row_dtypes = hfile[channel][datasets[0]].dtype

							# grab relevant datasets based on gps range
							relevant_datasets = [dataset for dataset in datasets if self.start_time <= int(dataset.split('_')[0]) <= self.end_time]

							if self.preloaded_triggers:
								self.triggerdict[channel] = numpy.array(hfile[channel][dataset])
							else:
								raise NotImplementedError

	def items(self):
		return self.triggerdict.items()

	def keys(self):
		return self.triggerdict.keys()

	def values(self):
		return self.triggerdict.values()

	def __len__(self):
		return len(self.triggerdict)

	def __getitem__(self, key):
		return self.triggerdict[key]

	def __contains__(self, key):
		return key in self.triggerdict

	def __repr__(self):
		return repr(self.triggerdict)

class InjectionData(object):

	def __init__(self, injection_set, start_time = None, end_time = None, file_format = 'xml', verbose = True, channel = None, waveform_type = 'all', ifo = 'H1'):
		"""
		Returns object with various injection parameters.
		"""
		self.injection_set = injection_set
		self.start_time = start_time
		self.end_time = end_time
		self.file_format = file_format
		self.waveform_type = waveform_type
		self.verbose = verbose
		self.channel = channel

		# set ifo to translate geocenter time to local time
		ifos = {
			'H1': lal.CachedDetectors[lal.LHO_4K_DETECTOR],
			'L1': lal.CachedDetectors[lal.LLO_4K_DETECTOR]
		}
		self.ifo = ifos[ifo]

		# populate data structure with relevant triggers
		self.triggerdict = {}
		self._extract_triggers()

	def filter_triggers(self, column = 'snr', threshold = 8.0):
		"""
		filter triggers in place based on some threshold value and a column,
		default is snr with a threshold of 8.0
		"""
		for key in self.triggerdict.keys():
			filter_idx = numpy.where(self.triggerdict[key][column] >= threshold)
			self.triggerdict[key] = self.triggerdict[key][filter_idx]

	def sort_triggers(self, column = 'snr', descending = True):
		"""
		returns a sorted list of descending triggers based on column,
		in the form of (row, key) where key = (gps_range, channel, rate)
		"""
		rows = [(row, key) for key, value in self.triggerdict.items() if value.shape[0] > 0 for row in numpy.split(value, value.shape[0], axis=0)]
		return [row for row in sorted(rows, key=lambda e: e[0][column][0], reverse=descending)]

	def _extract_triggers(self):
		"""
		helper method to extract triggers from disk given injection files
		"""
		# Define named structures for clarity
		Row = namedtuple('Row', ['trigger_time', 'snr', 'frequency', 'phase', 'q'])
		row_dtype = [(column, 'f8') for column in Row._fields]

		# define a content handler
		class LIGOLWContentHandler(ligolw.LIGOLWContentHandler):
		    pass

		lsctables.use_in(LIGOLWContentHandler)

		# load injections
		inj_file = ligolw_utils.load_filename(self.injection_set, contenthandler = LIGOLWContentHandler, verbose=self.verbose)
		inj_table = lsctables.SimBurstTable.get_table(inj_file)

		# preallocate space and initialize all values to nan
		rows = []

		# filter rows that don't span the gps range
		for row in inj_table:
			trg_time = row.time_geocent_gps + row.time_geocent_gps_ns * 1e-9

			if trg_time >= int(self.start_time) and trg_time <= int(self.end_time) + 1:
				rows.append(row)

		# optionally filter rows based on injection type
		if self.waveform_type == 'SG':
			rows = [row for row in rows if row.waveform == 'SineGaussian']
		elif self.waveform_type == 'WNB':
			rows = [row for row in rows if row.waveform == 'BTLWNB']

		# format rows and convert geocenter time to local time for injections
		# NOTE: SNR**2 is stored in amplitude column for these MDC injections
		shifted_rows = []
		for row in rows:
			geocent_time = row.time_geocent_gps + row.time_geocent_gps_ns * 1e-9
			ifo_dt = lal.TimeDelayFromEarthCenter(self.ifo.location, row.ra, row.dec, geocent_time)
			inj_time = numpy.float64(geocent_time) + numpy.float64(ifo_dt)
			trigger_row = Row(trigger_time = inj_time, snr = numpy.sqrt(row.amplitude), frequency = row.frequency, phase = 0, q = row.q)
			shifted_rows.append(trigger_row)

		# append to trigger dict
		self.triggerdict[self.channel] = numpy.array(shifted_rows, dtype=row_dtype)

		# convert to numpy recarray
		for channel in self.triggerdict.keys():
			self.triggerdict[channel] = self.triggerdict[channel].view(numpy.recarray)

	def items(self):
		return self.triggerdict.items()

	def keys(self):
		return self.triggerdict.keys()

	def values(self):
		return self.triggerdict.values()

	def __len__(self):
		return len(self.triggerdict)

	def __getitem__(self, key):
		return self.triggerdict[key]

	def __contains__(self, key):
		return key in self.triggerdict

	def __repr__(self):
		return repr(self.triggerdict)
