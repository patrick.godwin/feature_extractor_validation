__usage__ = "setup.py command [--options]"
__description__ = "standard install script"
__author__ = "Patrick Godwin (patrick.godwin@ligo.org)"

#-------------------------------------------------

from distutils.core import setup

setup(
	name = 'Feature eXtractor Validation TOOLS',
	version = '2.0',
	url = 'https://git.ligo.org/patrick.godwin/feature_extractor_validation',
	author = __author__,
	author_email = 'patrick.godwin@ligo.org',
	description = __description__,
	licence = 'LICENSE',
	scripts = [
		'bin/fxvtools-cluster',
		'bin/fxvtools-condor_cluster',
		'bin/fxvtools-validate',
	],
	packages = [
		'fxvtools',
	],
	requires = [
	]
)
