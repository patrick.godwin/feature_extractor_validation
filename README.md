# Feature Extractor Validation Tools (fxvtools)

This repository houses executables and utilities to validate the feature extraction pipeline.

Some comparison tools and plots generated have been heavily influenced by the ETG performance study
done in 2015, and parts of the legacy laldetchar repo where this ETG comparison code was housed have been
repurposed for use here.

Please contact Patrick Godwin (patrick.godwin@ligo.org) with questions.

### Resources
Links to the 2015 ETG comparison and results are posted below:

* https://wiki.ligo.org/DetChar/ETGperformanceStudy
* https://dcc.ligo.org/DocDB/0118/G1500276/004/ETGupdateForLVC.pdf